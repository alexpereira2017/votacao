# README #

### Pré-requisitos ###
* Docker

### Para executar a aplicação ###
* No desafio foi usado o banco H2, os dados são persistidos e ficam disponíveis após reinicio da aplicação (link para conexão direta http://localhost:8911/v1/h2-console).
* A porta configurada para uso na aplicação é a 8911.
* Os seguintes endpoints foram criados: 
    
 * Criação de nova pauta:
http://localhost:8911/v1/gerenciamento/nova-pauta/
``` json
{
    "nome": "Os investimentos devem ser realizados?"
}
```
   
* Abertura de sessão:
http://localhost:8911/v1/gerenciamento/abertura-sessao/
``` json
{
    "inicio": "2020-10-12T22:17:00",
    "pautaNumero": 1,
    "disponibilidade" : 5
}
```

   * Votação:
http://localhost:8911/v1/votacao/novo-voto/
``` json
{
    "cpf": "69718662820",
    "resposta": false,
    "sessaoNumero": 6
}
```

#### Subindo o RabitMQ ####
* Acessar o diretório do projeto onde se encontra o arquivo "docker-compose.yml" e no terminal executar:
$ docker-compose up -d

* Acesso ao gerenciador do RabbitQM: http://localhost:15672/
   * Usuário: guest
   * Senha: guest
