package com.lunacom.votacao.job;

import com.lunacom.votacao.message.VotacaoRelatorioMessage;
import com.lunacom.votacao.producer.RabbitMQProducer;
import com.lunacom.votacao.service.PautaService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class RelatorioJob implements Runnable {

    private final Integer pautaId;

    private final PautaService pautaService;

    private final RabbitMQProducer messageProducer;

    @Override
    public void run() {
        final VotacaoRelatorioMessage message = pautaService.gerarEstatisticas(pautaId);
        messageProducer.produce(message);
        log.info("Execução da tarefa agendada de término de sessão");
    }
}
