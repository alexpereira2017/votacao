package com.lunacom.votacao.client;

import com.lunacom.votacao.client.response.CpfStatusResponse;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Component
@AllArgsConstructor
public class CpfStatusClient {
    private RestTemplate restTemplate;
    private final String SERVICE_URL = "https://user-info.herokuapp.com";

    public ResponseEntity<CpfStatusResponse> consultar(String cpf) {
        try {
            return restTemplate.getForEntity(SERVICE_URL.concat("/users/{cpf}"), CpfStatusResponse.class, cpf);
        } catch (Exception e) {
            log.warn(String.format("Falha na pesquisa do cpf %s. Detalhe: %s", cpf, e.getMessage()));
        }
        return null;
    }
}
