package com.lunacom.votacao.client.response;

import enumeration.CpfStatusEnum;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CpfStatusResponse {
    private CpfStatusEnum status;
}
