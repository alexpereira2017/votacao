package com.lunacom.votacao.repository;

import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.VotoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface VotoRepository extends JpaRepository<VotoEntity, Integer> {

    List<VotoEntity> findAllByCpfAndSessao_Pauta(String cpfNumber, PautaEntity pauta);
}
