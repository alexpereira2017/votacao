package com.lunacom.votacao.repository;

import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;

public interface SessaoRepository extends JpaRepository<SessaoEntity, Integer> {

    Optional<SessaoEntity> findByPautaAndInicioIsBeforeAndEncerramentoIsAfter(PautaEntity pauta, LocalDateTime inicio, LocalDateTime encerramento);

    Optional<SessaoEntity> findByIdAndInicioIsBeforeAndEncerramentoIsAfter(Integer id, LocalDateTime inicio, LocalDateTime encerramento);

}
