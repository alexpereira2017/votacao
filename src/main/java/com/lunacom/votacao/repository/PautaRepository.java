package com.lunacom.votacao.repository;

import com.lunacom.votacao.repository.entity.PautaEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PautaRepository extends JpaRepository<PautaEntity, Integer> {
}
