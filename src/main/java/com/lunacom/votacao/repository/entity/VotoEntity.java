package com.lunacom.votacao.repository.entity;

import com.lunacom.votacao.service.SessaoService;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "voto")
public class VotoEntity implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String cpf;

    private boolean resposta;

    @ManyToOne
    @JoinColumn(name="sessao_id")
    private SessaoEntity sessao;
}
