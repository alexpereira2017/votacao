package com.lunacom.votacao.repository.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Data
@Entity
@Table(name = "pauta")
public class PautaEntity implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private String nome;

    @OneToMany(mappedBy = "pauta", fetch = FetchType.EAGER)
    private List<SessaoEntity> sessoes;
}
