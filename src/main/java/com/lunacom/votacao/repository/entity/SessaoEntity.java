package com.lunacom.votacao.repository.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
@Table(name = "sessao")
public class SessaoEntity implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;

    private LocalDateTime inicio;

    private LocalDateTime encerramento;

    private Integer disponibilidade;

    @ManyToOne
    @JoinColumn(name="pauta_id")
    private PautaEntity pauta;

    @OneToMany(mappedBy = "sessao", fetch = FetchType.EAGER)
    private List<VotoEntity> votos;
}
