package com.lunacom.votacao.message;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@ToString
@Getter
public class VotacaoRelatorioMessage {
    private String pauta;
    private long total;
    private long sim;
    private long nao;

    public VotacaoRelatorioMessage(
            @JsonProperty("pauta") String pauta,
            @JsonProperty("total") long total,
            @JsonProperty("sim") long sim,
            @JsonProperty("nao") long nao) {

        this.pauta = pauta;
        this.total = total;
        this.sim = sim;
        this.nao = nao;
    }
}
