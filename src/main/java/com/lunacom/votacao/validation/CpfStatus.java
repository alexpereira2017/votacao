package com.lunacom.votacao.validation;

import com.lunacom.votacao.validation.validators.CpfStatusValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = CpfStatusValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CpfStatus {
    String message() default "O CPF informado não está apto à votar";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
