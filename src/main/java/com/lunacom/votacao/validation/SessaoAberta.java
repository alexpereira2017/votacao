package com.lunacom.votacao.validation;

import com.lunacom.votacao.validation.validators.SessaoAbertaValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = SessaoAbertaValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface SessaoAberta {
    String message() default "Não há sessão aberta para a tentativa de voto";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
