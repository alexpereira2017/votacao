package com.lunacom.votacao.validation;

import com.lunacom.votacao.validation.validators.VotoDuplicadoValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = VotoDuplicadoValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface VotoDuplicado {
    String message() default "O CPF atual já votou na presente pauta";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
