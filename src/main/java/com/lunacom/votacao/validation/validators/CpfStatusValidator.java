package com.lunacom.votacao.validation.validators;

import com.lunacom.votacao.client.CpfStatusClient;
import com.lunacom.votacao.client.response.CpfStatusResponse;
import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.validation.CpfStatus;
import com.lunacom.votacao.validation.SessaoAberta;
import enumeration.CpfStatusEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

@RequiredArgsConstructor
@Component
public class CpfStatusValidator implements ConstraintValidator<CpfStatus, String> {

    private final CpfStatusClient cpfStatusClient;

    @Override
    public void initialize(CpfStatus constraintAnnotation) {

    }

    @Override
    public boolean isValid(String cpf, ConstraintValidatorContext constraintValidatorContext) {
        final ResponseEntity<CpfStatusResponse> result = cpfStatusClient.consultar(cpf);
        if (Objects.isNull(result)) {
            return false;
        }
        if (CpfStatusEnum.ABLE_TO_VOTE.equals(result.getBody().getStatus())) {
            return true;
        }
        return false;
    }

}
