package com.lunacom.votacao.validation.validators;

import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.service.SessaoService;
import com.lunacom.votacao.validation.SessaoAberta;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class SessaoAbertaValidator implements ConstraintValidator<SessaoAberta, NovoVotoRequest> {

    private final SessaoService sessaoService;

    @Override
    public void initialize(SessaoAberta constraintAnnotation) {

    }

    @Override
    public boolean isValid(NovoVotoRequest novoVotoRequest, ConstraintValidatorContext constraintValidatorContext) {
        final Optional<SessaoEntity> sessaoAbertaOptional = sessaoService.pesquisarSessaoAbertaAgora(novoVotoRequest);
        if (sessaoAbertaOptional.isPresent()) {
            return true;
        }
        return false;
    }
}
