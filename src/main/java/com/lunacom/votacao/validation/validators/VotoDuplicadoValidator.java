package com.lunacom.votacao.validation.validators;

import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.repository.entity.VotoEntity;
import com.lunacom.votacao.service.VotoService;
import com.lunacom.votacao.validation.VotoDuplicado;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

@RequiredArgsConstructor
@Component
public class VotoDuplicadoValidator implements ConstraintValidator<VotoDuplicado, NovoVotoRequest> {

    private final VotoService votoService;

    @Override
    public void initialize(VotoDuplicado constraintAnnotation) {

    }

    @Override
    public boolean isValid(NovoVotoRequest request, ConstraintValidatorContext constraintValidatorContext) {
        final List<VotoEntity> votoEntities =
                votoService.pesquisarVotosPorPauta(request.getSessaoNumero(), request.getCpf());
        if (votoEntities.isEmpty()) {
            return true;
        }
        return false;
    }
}
