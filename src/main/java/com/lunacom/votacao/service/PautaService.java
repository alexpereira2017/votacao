package com.lunacom.votacao.service;

import com.lunacom.votacao.controller.request.NovaPautaRequest;
import com.lunacom.votacao.message.VotacaoRelatorioMessage;
import com.lunacom.votacao.repository.PautaRepository;
import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.repository.entity.VotoEntity;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class PautaService {

    private final PautaRepository repo;

    private final ModelMapper mapper;

    public PautaEntity criar(@Valid NovaPautaRequest obj) {
        final PautaEntity entity = mapper.map(obj, PautaEntity.class);
        return repo.save(entity);
    }

    public PautaEntity pesquisarPorId(Integer id) throws ObjectNotFoundException {
        Optional<PautaEntity> obj = repo.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Não foi encontrado a pauta relacionada à pesquisa"));
    }

    public VotacaoRelatorioMessage gerarEstatisticas(Integer pautaId) {
        final Optional<PautaEntity> byId = repo.findById(pautaId);
        final PautaEntity pautaEntity = byId.get();

        final List<SessaoEntity> sessoes = pautaEntity.getSessoes();
        List<VotoEntity> list = sessoes
                                    .stream()
                                    .flatMap(s-> s.getVotos().stream())
                                    .collect(Collectors.toList());

        final long votosTotal = list.size();
        final long votosSim = list.stream().filter(x -> x.isResposta()).count();
        final long votosNao = votosTotal - votosSim;

        return VotacaoRelatorioMessage.builder().build()
                .builder()
                .pauta(pautaEntity.getNome())
                .total(votosTotal)
                .sim(votosSim)
                .nao(votosNao)
                .build();
    }
}
