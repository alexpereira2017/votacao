package com.lunacom.votacao.service;

import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.repository.VotoRepository;
import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.repository.entity.VotoEntity;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class VotoService {

    private final VotoRepository repo;

    private final SessaoService sessaoService;

    private final ModelMapper mapper;

    public void registrar(NovoVotoRequest request) {
        final VotoEntity entity = mapper.map(request, VotoEntity.class);
        final Optional<SessaoEntity> byId = sessaoService.findById(request.getSessaoNumero());
        if (byId.isPresent()) {
            entity.setSessao(byId.get());
        }
        repo.save(entity);
    }

    public List<VotoEntity> pesquisarVotosPorPauta(Integer sessaoNumero, String cpf) {
        final SessaoEntity sessaoEntity = sessaoService.findById(sessaoNumero)
                .orElseThrow(() -> new NoSuchElementException("Sessão não encontrada"));
        final PautaEntity pauta = sessaoEntity.getPauta();
        return repo.findAllByCpfAndSessao_Pauta(cpf, pauta);
    }
}
