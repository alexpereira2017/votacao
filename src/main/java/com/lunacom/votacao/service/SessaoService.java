package com.lunacom.votacao.service;

import com.lunacom.votacao.controller.request.AberturaSessaoRequest;
import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.job.RelatorioJob;
import com.lunacom.votacao.producer.RabbitMQProducer;
import com.lunacom.votacao.repository.SessaoRepository;
import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class SessaoService {

    private final SessaoRepository repo;

    private final PautaService pautaService;

    private TaskScheduler taskScheduler;

    private final ModelMapper mapper;

    private final RabbitMQProducer messageProducer;

    private final Integer disponibilitadePadrao;

    public SessaoService(SessaoRepository repo, PautaService pautaService, TaskScheduler taskScheduler, ModelMapper mapper,
                         RabbitMQProducer messageProducer, @Value("${sessao.disponibilitade-padrao}") Integer disponibilitadePadrao) {
        this.repo = repo;
        this.pautaService = pautaService;
        this.taskScheduler = taskScheduler;
        this.mapper = mapper;
        this.messageProducer = messageProducer;
        this.disponibilitadePadrao = disponibilitadePadrao;
    }

    public Optional<SessaoEntity> findById(Integer id) {
        return repo.findById(id);
    }

    public Optional<SessaoEntity> pesquisarSessaoAbertaAgora(NovoVotoRequest request) {
        final SessaoEntity sessaoEntity = findById(request.getSessaoNumero())
                .orElseThrow(() -> new NoSuchElementException("Sessão não encontrada"));

        final LocalDateTime now = LocalDateTime.now();
        return repo.findByPautaAndInicioIsBeforeAndEncerramentoIsAfter(sessaoEntity.getPauta(), now, now);
    }

    public SessaoEntity abrir(@Valid AberturaSessaoRequest obj) throws ObjectNotFoundException {
        final SessaoEntity entity = mapper.map(obj, SessaoEntity.class);
        final PautaEntity pautaEntity = pautaService.pesquisarPorId(obj.getPautaNumero());
        entity.setPauta(pautaEntity);
        if (Objects.isNull(entity.getDisponibilidade())) {
            entity.setDisponibilidade(disponibilitadePadrao);
        }
        entity.setEncerramento(obj.getInicio().plusMinutes(obj.getDisponibilidade()));
        agendarEnvioRelatorio(entity);
        return repo.save(entity);
    }

    private void agendarEnvioRelatorio(SessaoEntity entity) {
        final Instant instant = entity.getInicio()
                .atZone(ZoneId.of("America/Sao_Paulo")).toInstant().plusSeconds(entity.getDisponibilidade() * 60);
        taskScheduler.schedule(new RelatorioJob(entity.getPauta().getId(), pautaService, messageProducer), instant);
        log.info(String.format(">>>> Relatorio da pauta %s agendado para o final da sessao %s",
                entity.getPauta().getNome(), instant.toString()));
//        final RelatorioJob job = new RelatorioJob(entity, pautaService, messageProducer);
//        job.run();
    }

}
