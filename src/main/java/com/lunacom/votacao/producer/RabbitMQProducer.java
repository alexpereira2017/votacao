package com.lunacom.votacao.producer;

import com.lunacom.votacao.message.VotacaoRelatorioMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class RabbitMQProducer {

    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${rabbitmq.exchange}")
    private String exchange;

    @Value("${rabbitmq.routingkey}")
    private String routingkey;

    public void produce(VotacaoRelatorioMessage message){
        amqpTemplate.convertAndSend(exchange, routingkey, message);
        log.info("Enviando mensagem... " + message);
    }

}
