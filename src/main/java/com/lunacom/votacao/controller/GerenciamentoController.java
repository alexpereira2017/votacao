package com.lunacom.votacao.controller;

import com.lunacom.votacao.controller.request.AberturaSessaoRequest;
import com.lunacom.votacao.controller.request.NovaPautaRequest;
import com.lunacom.votacao.controller.response.UsuarioResponse;
import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.service.PautaService;
import com.lunacom.votacao.service.SessaoService;
import com.lunacom.votacao.util.DataUtil;
import javassist.tools.rmi.ObjectNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value="/gerenciamento")
public class GerenciamentoController {

    private HttpHeaders headers = new HttpHeaders();

    private final PautaService pautaService;
    private final SessaoService sessaoService;

    @RequestMapping(value="/nova-pauta", method= RequestMethod.POST)
    public ResponseEntity<UsuarioResponse> criarPauta(@Valid @RequestBody final NovaPautaRequest obj) {
        final PautaEntity entity = pautaService.criar(obj);
        final UsuarioResponse response = UsuarioResponse
                .builder()
                .mensagem(String.format("Nova pauta criada com o código %s", entity.getId()))
                .build();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }

    @RequestMapping(value="/abertura-sessao", method= RequestMethod.POST)
    public ResponseEntity<UsuarioResponse> abrirSessao(@Valid @RequestBody AberturaSessaoRequest obj) throws ObjectNotFoundException {
        final SessaoEntity entity = sessaoService.abrir(obj);
        final UsuarioResponse response = UsuarioResponse
                .builder()
                .mensagem(String.format("Sessão aberta com início em %s e término em %s. Código (sessaoNumero) para acessar e votar: %s",
                        entity.getInicio().format(DataUtil.formatterUsuarioFinal),
                        entity.getEncerramento().format(DataUtil.formatterUsuarioFinal),
                        entity.getId()))
                .build();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(response);
    }
}
