package com.lunacom.votacao.controller.request;

import com.lunacom.votacao.validation.CpfStatus;
import com.lunacom.votacao.validation.SessaoAberta;
import com.lunacom.votacao.validation.VotoDuplicado;
import lombok.Data;

import javax.validation.constraints.NotNull;

@SessaoAberta
@VotoDuplicado
@Data
public class NovoVotoRequest {

    @CpfStatus
    @NotNull(message="O documento de identificação deve ser informado")
    private String cpf;

    @NotNull(message="O voto deve ser informado")
    private Boolean resposta;

    @NotNull(message = "O código da sessão é obrigatório")
    private Integer sessaoNumero;
}
