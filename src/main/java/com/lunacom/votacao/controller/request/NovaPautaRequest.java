package com.lunacom.votacao.controller.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class NovaPautaRequest {

    @NotEmpty(message="Informe o nome do processo de votação(Pauta)")
    private String nome;
}
