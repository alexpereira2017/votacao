package com.lunacom.votacao.controller.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UsuarioResponse {
    private String mensagem;
}
