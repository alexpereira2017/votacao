package com.lunacom.votacao.controller;

import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.controller.response.UsuarioResponse;
import com.lunacom.votacao.service.VotoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value="/votacao")
public class VotacaoController {
    private HttpHeaders headers = new HttpHeaders();

    private final VotoService votoService;

    @RequestMapping(value="/novo-voto", method= RequestMethod.POST)
    public ResponseEntity<UsuarioResponse> registrarVoto(@Valid @RequestBody NovoVotoRequest obj) {
        votoService.registrar(obj);
        final UsuarioResponse response = UsuarioResponse
                .builder().mensagem("Voto registrado com sucesso!").build();
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .headers(headers)
                .body(response);
    }
}
