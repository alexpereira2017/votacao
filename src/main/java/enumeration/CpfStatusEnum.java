package enumeration;

public enum CpfStatusEnum {
    UNABLE_TO_VOTE, ABLE_TO_VOTE
}
