package com.lunacom.votacao.service;

import com.lunacom.votacao.builder.PautaBuilder;
import com.lunacom.votacao.builder.RequestBuilder;
import com.lunacom.votacao.builder.SessaoBuilder;
import com.lunacom.votacao.controller.request.AberturaSessaoRequest;
import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.producer.RabbitMQProducer;
import com.lunacom.votacao.repository.SessaoRepository;
import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import javassist.tools.rmi.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class SessaoServiceTest {

    SessaoService service;

    @MockBean
    SessaoRepository repo;

    @MockBean
    PautaService pautaService;

    @MockBean
    private TaskScheduler taskScheduler;

    ModelMapper mapper;

    @MockBean
    RabbitMQProducer messageProducer;

    Integer disponibilitadePadrao = 1;

    private final Integer id = 1;

    @BeforeEach
    public void setUp() {
        this.mapper = new ModelMapper();
        this.service = new SessaoService(repo, pautaService, taskScheduler, mapper, messageProducer, disponibilitadePadrao);
    }

    @Test
    @DisplayName("Deve pesquisar por Id com sucesso")
    public void findByIdTest() {
        final SessaoEntity sessaoEntity = SessaoBuilder.criarSessaoEntity();
        when(repo.findById(id)).thenReturn(Optional.of(sessaoEntity));

        final Optional<SessaoEntity> response = service.findById(id);

        assertThat(response).containsInstanceOf(SessaoEntity.class);
    }

    @Test
    @DisplayName("Deve pesquisar por Sessão de Votação aberta com sucesso")
    public void pesquisarSessaoAbertaAgoraTest() {
        final SessaoEntity sessaoEntity = SessaoBuilder.criarSessaoEntityComPauta();
        when(repo.findById(id)).thenReturn(Optional.of(sessaoEntity));
        when(repo.findByPautaAndInicioIsBeforeAndEncerramentoIsAfter(any(PautaEntity.class),
                any(LocalDateTime.class), any(LocalDateTime.class))).thenReturn(Optional.of(sessaoEntity));
        final NovoVotoRequest novoVotoRequest = RequestBuilder.criarNovoVotoRequest();

        final Optional<SessaoEntity> response = service.pesquisarSessaoAbertaAgora(novoVotoRequest);
        assertThat(response).containsInstanceOf(SessaoEntity.class);
        assertThat(response).isPresent();
    }

    @Test
    @DisplayName("Deve pesquisar por Sessão de Votação aberta com sucesso")
    public void pesquisarSessaoAbertaAgoraComExceptionTest() {
        final SessaoEntity sessaoEntity = SessaoBuilder.criarSessaoEntityComPauta();
        when(repo.findById(id)).thenReturn(Optional.empty());
        final NovoVotoRequest novoVotoRequest = RequestBuilder.criarNovoVotoRequest();

        final Throwable throwable = catchThrowable(() -> service.pesquisarSessaoAbertaAgora(novoVotoRequest));

        assertThat(throwable)
                .isInstanceOf(NoSuchElementException.class)
                .hasFieldOrPropertyWithValue("detailMessage", "Sessão não encontrada");
    }

    @Test
    @DisplayName("Deve abrir Sessão de Votação com sucesso")
    public void abrirTest() throws ObjectNotFoundException {
        final PautaEntity pautaEntity = PautaBuilder.criarPautaEntity();
        final AberturaSessaoRequest request = RequestBuilder.criarAberturaSessaoRequest();
        when(pautaService.pesquisarPorId(request.getPautaNumero())).thenReturn(pautaEntity);

        service.abrir(request);

        verify(taskScheduler, times(1)).schedule(any(Runnable.class), any(Instant.class));
    }
}
