package com.lunacom.votacao.service;

import com.lunacom.votacao.builder.RequestBuilder;
import com.lunacom.votacao.builder.SessaoBuilder;
import com.lunacom.votacao.builder.VotoBuilder;
import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.repository.VotoRepository;
import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.repository.entity.VotoEntity;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class VotoServiceTest {
    VotoService service;

    @MockBean
    VotoRepository repo;

    @MockBean
    SessaoService sessaoService;

    ModelMapper mapper;

    @BeforeEach
    public void setUp() {
        this.mapper = new ModelMapper();
        this.service = new VotoService(repo, sessaoService, mapper);
    }

    @Test
    @DisplayName("Deve registrar com sucesso")
    public void registrarTest() {
        final NovoVotoRequest request = RequestBuilder.criarNovoVotoRequest();
        final SessaoEntity sessaoEntity = SessaoBuilder.criarSessaoEntity();
        when(sessaoService.findById(request.getSessaoNumero())).thenReturn(Optional.of(sessaoEntity));

        service.registrar(request);

        verify(repo, times(1)).save(any(VotoEntity.class));
    }

    @Test
    @DisplayName("Deve pesquisar por voto de um determinado CPF em uma pauta")
    public void pesquisarVotosPorPautaTest() {
        final NovoVotoRequest request = RequestBuilder.criarNovoVotoRequest();
        final SessaoEntity sessaoEntity = SessaoBuilder.criarSessaoEntityComPauta();
        final List<VotoEntity> list = Arrays.asList(VotoBuilder.criarVotoSimEntity(), VotoBuilder.criarVotoNaoEntity());
        when(sessaoService.findById(request.getSessaoNumero())).thenReturn(Optional.of(sessaoEntity));
        when(repo.findAllByCpfAndSessao_Pauta(anyString(), any(PautaEntity.class))).thenReturn(list);

        final List<VotoEntity> response = service.pesquisarVotosPorPauta(1, "52968136500");

        assertThat(response.size()).isEqualTo(2);
    }

    @Test
    @DisplayName("Deve lançar exceção NoSuchElementException")
    public void pesquisarVotosPorPautaLancarNoSuchElementExceptionTest() {
        final NovoVotoRequest request = RequestBuilder.criarNovoVotoRequest();
        when(sessaoService.findById(request.getSessaoNumero())).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> service.pesquisarVotosPorPauta(1, "52968136500"));

        assertThat(throwable)
                .isInstanceOf(NoSuchElementException.class)
                .hasFieldOrPropertyWithValue("detailMessage", "Sessão não encontrada");
    }
}
