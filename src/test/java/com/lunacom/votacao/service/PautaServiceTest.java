package com.lunacom.votacao.service;

import com.lunacom.votacao.builder.PautaBuilder;
import com.lunacom.votacao.builder.RequestBuilder;
import com.lunacom.votacao.controller.request.NovaPautaRequest;
import com.lunacom.votacao.message.VotacaoRelatorioMessage;
import com.lunacom.votacao.repository.PautaRepository;
import com.lunacom.votacao.repository.entity.PautaEntity;
import javassist.tools.rmi.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class PautaServiceTest {

    PautaService service;

    @MockBean
    PautaRepository repo;

    private ModelMapper mapper;

    private final Integer id = 1;

    @BeforeEach
    public void setUp() {
        this.mapper = new ModelMapper();
        this.service = new PautaService(repo, mapper);
    }

    @Test
    @DisplayName("Deve criar uma pauta com sucesso")
    public void criarTest() {
        final NovaPautaRequest request = RequestBuilder.criarNovaPautaRequest();
        when(repo.save(any(PautaEntity.class))).thenReturn(PautaBuilder.criarPautaEntity());

        final PautaEntity response = service.criar(request);

        assertThat(response.getId()).isEqualTo(1);
        assertThat(response.getNome()).isEqualTo("Controlar finanças online?");
    }

    @Test
    @DisplayName("Deve pesquisar por ID com sucesso")
    public void pesquisarPorIdTest() throws ObjectNotFoundException {
        final PautaEntity pautaEntity = PautaBuilder.criarPautaEntity();
        when(repo.findById(id)).thenReturn(Optional.of(pautaEntity));

        final PautaEntity response = service.pesquisarPorId(id);

        assertThat(response.getId()).isEqualTo(1);
        assertThat(response.getNome()).isEqualTo("Controlar finanças online?");
    }

    @Test
    @DisplayName("Deve pesquisar por ID e lançar uma exceção")
    public void pesquisarPorIdObjectNotFoundExceptionTest() {
        when(repo.findById(id)).thenReturn(Optional.empty());

        final Throwable throwable = catchThrowable(() -> service.pesquisarPorId(id));
        assertThat(throwable)
                .isInstanceOf(ObjectNotFoundException.class)
                .hasFieldOrPropertyWithValue("detailMessage", "Não foi encontrado a pauta relacionada à pesquisa is not exported");
    }

    @Test
    @DisplayName("Deve gerar relatorio de estatisticas com sucesso")
    public void gerarEstatisticasTest() throws ObjectNotFoundException {
        final PautaEntity pautaEntity = PautaBuilder.criarPautaEntityCompleto();
        when(repo.findById(id)).thenReturn(Optional.of(pautaEntity));

        final VotacaoRelatorioMessage response = service.gerarEstatisticas(id);

        assertThat(response.getPauta()).isEqualTo("Controlar finanças online?");
        assertThat(response.getTotal()).isEqualTo(2);
        assertThat(response.getSim()).isEqualTo(1);
        assertThat(response.getNao()).isEqualTo(1);

    }


}
