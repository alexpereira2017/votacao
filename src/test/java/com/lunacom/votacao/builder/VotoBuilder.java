package com.lunacom.votacao.builder;

import com.lunacom.votacao.repository.entity.VotoEntity;

public class VotoBuilder {
    public static VotoEntity criarVotoSimEntity() {
        final VotoEntity entity = new VotoEntity();
        entity.setCpf("85838652306");
        entity.setResposta(true);
        return entity;
    }
    public static VotoEntity criarVotoNaoEntity() {
        final VotoEntity entity = new VotoEntity();
        entity.setCpf("52234687675");
        entity.setResposta(false);
        return entity;
    }
}
