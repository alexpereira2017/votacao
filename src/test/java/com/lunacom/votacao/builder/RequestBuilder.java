package com.lunacom.votacao.builder;

import com.lunacom.votacao.controller.request.AberturaSessaoRequest;
import com.lunacom.votacao.controller.request.NovaPautaRequest;
import com.lunacom.votacao.controller.request.NovoVotoRequest;

import java.time.LocalDateTime;
import java.time.Month;

public class RequestBuilder {

    private static final LocalDateTime inicio = LocalDateTime.of(2020, Month.OCTOBER, 10, 19, 30, 00);
    private static final LocalDateTime termino = LocalDateTime.of(2020, Month.OCTOBER, 10, 20, 30, 00);

    public static NovaPautaRequest criarNovaPautaRequest() {
        return NovaPautaRequest
                .builder()
                .nome("Controlar finanças online?")
                .build();
    }

    public static NovoVotoRequest criarNovoVotoRequest() {
        NovoVotoRequest request = new NovoVotoRequest();
        request.setCpf("52968136500");
        request.setResposta(true);
        request.setSessaoNumero(1);
        return request;
    }

    public static AberturaSessaoRequest criarAberturaSessaoRequest() {
        AberturaSessaoRequest request = new AberturaSessaoRequest();
        request.setPautaNumero(1);
        request.setInicio(inicio);
        request.setDisponibilidade(60);
        return request;
    }
}
