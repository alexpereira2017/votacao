package com.lunacom.votacao.builder;

import com.lunacom.votacao.repository.entity.SessaoEntity;

import java.time.LocalDateTime;
import java.time.Month;

public class SessaoBuilder {

    private static final LocalDateTime inicio = LocalDateTime.of(2020, Month.OCTOBER, 10, 19, 30, 00);
    private static final LocalDateTime termino = LocalDateTime.of(2020, Month.OCTOBER, 10, 22, 30, 00);

    public static SessaoEntity criarSessaoEntity() {
        final SessaoEntity entity = new SessaoEntity();
        entity.setId(1);
        entity.setInicio(inicio);
        entity.setEncerramento(termino);
        entity.setDisponibilidade(150);
        return entity;
    }

    public static SessaoEntity criarSessaoEntityComPauta() {
        final SessaoEntity entity = new SessaoEntity();
        entity.setId(1);
        entity.setInicio(inicio);
        entity.setEncerramento(termino);
        entity.setDisponibilidade(150);
        entity.setPauta(PautaBuilder.criarPautaEntity());
        return entity;
    }
}
