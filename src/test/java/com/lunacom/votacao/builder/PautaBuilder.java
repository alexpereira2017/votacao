package com.lunacom.votacao.builder;

import com.lunacom.votacao.repository.entity.PautaEntity;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.repository.entity.VotoEntity;

import java.util.Arrays;

public class PautaBuilder {
    public static PautaEntity criarPautaEntity() {
        PautaEntity entity = new PautaEntity();
        entity.setId(1);
        entity.setNome("Controlar finanças online?");
        return entity;
    }

    public static PautaEntity criarPautaEntityCompleto() {
        final PautaEntity pautaEntity = criarPautaEntity();
        final SessaoEntity sessaoEntity = SessaoBuilder.criarSessaoEntity();
        final VotoEntity sim = VotoBuilder.criarVotoSimEntity();
        final VotoEntity nao = VotoBuilder.criarVotoNaoEntity();
        sessaoEntity.setVotos(Arrays.asList(sim, nao));
        pautaEntity.setSessoes(Arrays.asList(sessaoEntity));
        return pautaEntity;
    }
}
