package com.lunacom.votacao.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lunacom.votacao.builder.PautaBuilder;
import com.lunacom.votacao.builder.RequestBuilder;
import com.lunacom.votacao.builder.SessaoBuilder;
import com.lunacom.votacao.controller.request.AberturaSessaoRequest;
import com.lunacom.votacao.controller.request.NovaPautaRequest;
import com.lunacom.votacao.service.PautaService;
import com.lunacom.votacao.service.SessaoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@WebMvcTest(controllers = GerenciamentoController.class)
public class GerenciamentoControllerTest {
    @Autowired
    MockMvc mvc;

    @MockBean
    PautaService pautaService;

    @MockBean
    SessaoService sessaoService;

    static final String URL = "/gerenciamento";
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Test
    @DisplayName("Deve criar nova pauta com sucesso")
    public void criarPautaTest() throws Exception {
        final NovaPautaRequest requestBody = RequestBuilder.criarNovaPautaRequest();
        final String json = objectMapper.writeValueAsString(requestBody);

        BDDMockito.given( pautaService.criar(any())).willReturn(PautaBuilder.criarPautaEntity());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(URL.concat("/nova-pauta"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(json);

        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("mensagem").value("Nova pauta criada com o código 1"));
    }

    @Test
    @DisplayName("Deve abrir sessão com sucesso")
    public void abrirSessaoTest() throws Exception {
        final AberturaSessaoRequest requestBody = RequestBuilder.criarAberturaSessaoRequest();
        final String json = objectMapper.writeValueAsString(requestBody);

        BDDMockito.given( sessaoService.abrir(any())).willReturn(SessaoBuilder.criarSessaoEntity());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(URL.concat("/abertura-sessao"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(json);

        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("mensagem").value("Sessão aberta com início em 19:30 10/10/2020 e término em 22:30 10/10/2020. Código (sessaoNumero) para acessar e votar: 1"));
    }


}
