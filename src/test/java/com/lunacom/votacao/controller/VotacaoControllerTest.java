package com.lunacom.votacao.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lunacom.votacao.builder.RequestBuilder;
import com.lunacom.votacao.client.CpfStatusClient;
import com.lunacom.votacao.client.response.CpfStatusResponse;
import com.lunacom.votacao.controller.request.NovoVotoRequest;
import com.lunacom.votacao.repository.entity.SessaoEntity;
import com.lunacom.votacao.repository.entity.VotoEntity;
import com.lunacom.votacao.service.SessaoService;
import com.lunacom.votacao.service.VotoService;
import enumeration.CpfStatusEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.Mockito.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@WebMvcTest(controllers = VotacaoController.class)
public class VotacaoControllerTest {
    @Autowired
    MockMvc mvc;

    @MockBean
    VotoService service;

    @Autowired
    HttpServletRequest request;

    @MockBean
    CpfStatusClient cpfStatusClient;

    @MockBean
    SessaoService sessaoService;

    static final String URL = "/votacao";
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    @Test
    @DisplayName("Deve conseguir votar com sucesso")
    public void registrarVotoTest() throws Exception {
        final NovoVotoRequest requestBody = RequestBuilder.criarNovoVotoRequest();
        final String json = objectMapper.writeValueAsString(requestBody);

        final CpfStatusResponse cpfStatusResponse = new CpfStatusResponse();
        cpfStatusResponse.setStatus(CpfStatusEnum.ABLE_TO_VOTE);
        final ResponseEntity<CpfStatusResponse> responseEntity = new ResponseEntity<>(cpfStatusResponse, HttpStatus.OK);
        BDDMockito.given( cpfStatusClient.consultar(any())).willReturn(responseEntity);

        BDDMockito.given( sessaoService.pesquisarSessaoAbertaAgora(any())).willReturn(Optional.of(new SessaoEntity()));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(URL.concat("/novo-voto"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(json);

        mvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("mensagem").value("Voto registrado com sucesso!"));
    }

    @Test
    @DisplayName("Deve testar as mensages de retorno da validação")
    public void validacaoTest() throws Exception {
        final NovoVotoRequest requestBody = RequestBuilder.criarNovoVotoRequest();
        final String json = objectMapper.writeValueAsString(requestBody);

        final CpfStatusResponse cpfStatusResponse = new CpfStatusResponse();
        cpfStatusResponse.setStatus(CpfStatusEnum.UNABLE_TO_VOTE);
        final ResponseEntity<CpfStatusResponse> responseEntity = new ResponseEntity<>(cpfStatusResponse, HttpStatus.OK);
        BDDMockito.given( cpfStatusClient.consultar(any())).willReturn(responseEntity);

        BDDMockito.given( sessaoService.pesquisarSessaoAbertaAgora(any())).willReturn(Optional.empty());

        BDDMockito.given( service.pesquisarVotosPorPauta(any(), any())).willReturn(Arrays.asList(new VotoEntity()));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(URL.concat("/novo-voto"))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("utf-8")
                .content(json);

        mvc.perform(request)
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("mensagem").value("Verifique os seguintes itens antes de avançar"))
                .andExpect(jsonPath("detalhe").isArray());
    }
}
